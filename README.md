Kredit Laufzeit for Android
===========================

(c) 2011-2015 by Markus Hoffmann <kollo@users.sourceforge.net>

Rechnet die Kreditlaufzeit aus.

Dieses kleine Programm erlaubt es Ihnen auszurechnen, wieviel Kredit Sie sich
bei gegebenem effektiven Jahreszinsssatz und gewünschter monatlicher Belastung
leisten können. Die grafische Darstellung zeigt die Laufzeit in Abhängigkeit
von der Kreditsumme. Der maximale Kreditrahmen ist diejenige Summe, welche Sie
auch in beliebig vielen Jahren nicht zurückzahlen können.  Beachten Sie, daß
Ihre Bank Ihnen nur eine Summe leihen wird, welche Sie in maximal  35 Jahren
oder noch zu Ihren voraussichtlichen Lebzeiten zurückzahlen können.  Diese
Summe entnehmen Sie bitte dem Diagramm. Die Einstellungen erreichen Sie über
die Menu-Taste. Dort können die Ihre monatliche Zahlung einstellen, sowie eine
mögliche Extratilgung am Ende jedes Jahres. Beachten Sie den großen Einfluß, 
den dies hat.

	
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
