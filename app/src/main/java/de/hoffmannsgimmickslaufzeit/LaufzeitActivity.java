package de.hoffmannsgimmickslaufzeit;

/* LaufzeitActivity.java (c) 2011-2015 by Markus Hoffmann 
 *
 * This file is part of KreditLaufzeit for Android 
 * ===========================================================
 * KreditLaufzeit for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

public class LaufzeitActivity extends Activity {
	private static final String TAG = "Laufzeit";
	public static float zins=5;
	public static float extra=0;
	public static float belastung=1000;
	public static int maxkap=1000000;
	public static double data[]=new double[1600];
	private static TextView text1,text2;
	private static PlotView plot;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		text1=(TextView) findViewById(R.id.text1);
		text2=(TextView) findViewById(R.id.text2);
		plot=(PlotView) findViewById(R.id.plot);
	}
	@Override
	public void onResume() {
		super.onResume();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		try {
			zins=(float)Double.parseDouble(prefs.getString("zinssatz", "3.2"));
			extra=(float)Double.parseDouble(prefs.getString("extratilgung", "1000"));
			belastung=(float)Double.parseDouble(prefs.getString("belastung", "800"));
			redraw();
		} catch (NumberFormatException e) {
			Log.e(TAG,"Number ",e);
			text1.setText("Illegal Numbers!");
		}
	}
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	Intent viewIntent;
        switch (item.getItemId()) {
            case R.id.infotext:    
            	viewIntent = new Intent(LaufzeitActivity.this, AboutActivity.class);
            	startActivity(viewIntent);
            	break;
            case R.id.text:     
            	viewIntent = new Intent(LaufzeitActivity.this, SettingsActivity.class);
            	startActivity(viewIntent);
            	break;
            case R.id.finish: 
            	finish();
                break;
            default: 
            	return super.onOptionsItemSelected(item);
        }
        return true;
    }
    public static void redraw() {
    	int i;
    	for(i=0;i<1000;i++) {
		data[i]=lz((double)i*1000);
		if(data[i]<0) break;
        }
    	maxkap=(i-1)*1000;
    	plot.setRange(0, maxkap, 0, 35);
    	if(maxkap<10000) plot.setGrid(1000,5);
    	else if(maxkap<20000) plot.setGrid(5000,5);
    	else if(maxkap<100000) plot.setGrid(10000,5);
    	else if(maxkap<200000) plot.setGrid(50000,5);
    	else if(maxkap<1000000) plot.setGrid(100000,5);
    	
    	text1.setText(""+zins+"% eff Zins; Mtl: "+belastung+"€; Extra: "+extra+"€");
    	//text1.postInvalidate();
    	text2.setText((String)"==> max. Kredit: "+maxkap+"€");
    	//text2.postInvalidate();
    	//plot.postInvalidate();
    }
    public static double lz(double gesamt) {
    	int i=0;
    	double z;
    	
    	z=gesamt*zins/12.0/100;
    	while(true) {
    		if(12*z>12*belastung+extra) return -1;
    		if(z>=belastung) return -1;
    		gesamt-=(belastung-z);
    		i++;
    		if(gesamt<1000) break;
    		if((i%12)==0) gesamt-=extra;
    	}
    	return (double)i/12.0;
    }
}
