package de.hoffmannsgimmickslaufzeit;

/* SettingsActivity.java (c) 2011-2015 by Markus Hoffmann 
 *
 * This file is part of KreditLaufzeit for Android 
 * ===========================================================
 * KreditLaufzeit for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class SettingsActivity extends PreferenceActivity  implements OnSharedPreferenceChangeListener  {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		findPreference("about_version").setSummary(applicationVersion());

		for(int i=0;i<getPreferenceScreen().getPreferenceCount();i++){
			initSummary(getPreferenceScreen().getPreference(i));
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		// Set up a listener whenever a key changes             
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		// Unregister the listener whenever a key changes             
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) { 
		updatePrefSummary(findPreference(key));
	}

	private void initSummary(Preference p) {
		if (p instanceof PreferenceCategory) {
			PreferenceCategory pCat = (PreferenceCategory)p;
			for(int i=0;i<pCat.getPreferenceCount();i++) initSummary(pCat.getPreference(i));
		} else updatePrefSummary(p);
	}

	private void updatePrefSummary(Preference p) {
		if (p instanceof ListPreference) {
			ListPreference listPref = (ListPreference) p; 
			p.setSummary(listPref.getEntry()); 
		}
		if (p instanceof EditTextPreference) {
			EditTextPreference editTextPref = (EditTextPreference) p; 
			p.setSummary(editTextPref.getText()); 
		}
	}

	private final String applicationVersion() {
		try {return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;}
		catch (NameNotFoundException x)  {return "unknown";}
	}

	@Override
	public boolean onPreferenceTreeClick(final PreferenceScreen preferenceScreen, final Preference preference)  {
		final String key = preference.getKey();
		if ("about_license".equals(key)) {
			//              startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.LICENSE_URL)));
			finish();
		} else if ("about_version".equals(key)) {
			showDialog(1);
		} else if ("about_copyright".equals(key)) {
			showDialog(0);
		} else if ("about_help".equals(key)) {
			showDialog(2);
		} else if ("about_source".equals(key)) {
			//              startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.SOURCE_URL)));
			finish();
		} else if ("about_market_app".equals(key)) {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("market://details?id=%s", getPackageName()))));
			finish();
		} else if ("about_market_publisher".equals(key))  {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:\"Markus Hoffmann\"")));
			finish();
		}
		return false;
	}

	private Dialog scrollableDialog(String title, String text) {
		final Dialog dialog = new Dialog(this);
		if(title==null || title=="") dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(R.layout.maindialog);
		if(title!=null && title!="") dialog.setTitle(title);
		final TextView wV= (TextView) dialog.findViewById(R.id.TextView01);
		wV.setText(Html.fromHtml(text));
		//set up button
		final Button button = (Button) dialog.findViewById(R.id.Button01);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		return dialog;
	}

	@Override
	protected Dialog onCreateDialog(final int id) {
		//	if(id==2)      return scrollableDialog("",getResources().getString(R.string.hardware_vendor_info));
		//	else if(id==1) return scrollableDialog("",getResources().getString(R.string.helpdialog));
		//	else 
		if(id==1) return scrollableDialog("",getResources().getString(R.string.news)+
				getResources().getString(R.string.impressum));
		else if(id==0) return scrollableDialog("",getResources().getString(R.string.impressum));
		else if(id==2) return scrollableDialog(getResources().getString(R.string.word_anleitung),getResources().getString(R.string.readme));
		else return null;
	}
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId())  {
		// case android.R.id.home:
		//	finish();
		//	return true;
		default: 
			return super.onOptionsItemSelected(item);
		}
	}
}
