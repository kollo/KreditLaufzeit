package de.hoffmannsgimmickslaufzeit;

/* PlotView.java (c) 2011-2015 by Markus Hoffmann 
 *
 * This file is part of KreditLaufzeit for Android 
 * ===========================================================
 * KreditLaufzeit for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING/LICENSE for details
 */


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class PlotView extends View {
	private Paint paint;
	private int bx,by,bw,bh;
	private int sw,sh;
	private double xstep=100000,ystep=5;
	private String ex="Kapital [€]",ey="Laufzeit [Jahre]";
	private double xmin=0,xmax=1500000,ymin=0,ymax=40;
        private float textsize=16;

	public void setRange(double x1,double x2,double y1,double y2) {
		xmin=x1;xmax=x2;ymin=y1;ymax=y2;
	}
	public void setGrid(double gx, double gy) {
		xstep=gx;ystep=gy;
	}

	public PlotView(Context context) {
		super(context);
		initPlotView();
	}

	public PlotView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPlotView();
	}

	public PlotView(Context context, AttributeSet attrs, int defaultStyle) {
		super(context, attrs, defaultStyle);
		initPlotView();
	}
	protected void initPlotView() {
		setFocusable(true);
		paint=new Paint();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final int modex=MeasureSpec.getMode(widthMeasureSpec);
		final int sizex=MeasureSpec.getSize(widthMeasureSpec);
		final int modey=MeasureSpec.getMode(heightMeasureSpec);
		final int sizey=MeasureSpec.getSize(heightMeasureSpec);
		if (modex == MeasureSpec.UNSPECIFIED) sw=280;
		else sw=sizex;
		if (modey == MeasureSpec.UNSPECIFIED) sh=200;
		else sh=sizey;
		setMeasuredDimension(sw, sh);
	}
	private int kx(double dux)  {return bx+(int)((dux-xmin)/(xmax-xmin)*bw);}
	private int ky(double duy)  {return by+bh-(int)((duy-ymin)/(ymax-ymin)*bh);}

	@Override
	protected void onDraw(Canvas canvas) {
		double xs,ys,x,y;
		int voi=0,i;
		String xss,x2s;
		sw=getMeasuredWidth();
		sh=getMeasuredHeight();
		bw=sw*9/10;
		bh=sh*9/10;
		bx=sw*1/10;

		canvas.drawColor(Color.BLACK);
		paint.setStyle(Paint.Style.STROKE);
		paint.setColor(Color.WHITE);
		paint.setTextSize(16);
		canvas.drawRect(bx,by,bx+bw-1,by+bh-1, paint);
		if(xmin>xmax) xs=xmin; else xs=xmax;
		if(ymin>ymax) ys=ymin; else ys=ymax;

		for(x=0;x<xs;x+=xstep) {
			paint.setColor(Color.GRAY);	
			if(x>xmin && x<xmax) canvas.drawLine(kx(x),ky(ymin),kx(x),ky(ymax), paint);
			if(-x>xmin && -x<xmax) canvas.drawLine(kx(-x),ky(ymin),kx(-x),ky(ymax), paint);
			paint.setColor(Color.WHITE);
			paint.setAntiAlias(true);
			if(x>xmin && x<xmax) canvas.drawLine(kx(x),ky(0)+2,kx(x),ky(0)-2, paint);
			if(-x>xmin && -x<xmax) canvas.drawLine(kx(-x),ky(0)+2,kx(-x),ky(0)-2,paint);
			xss=""+x;
			x2s=""+(-x);
			if(kx(x)-(int)paint.measureText(xss)/2>voi+2) {
				voi=(int)paint.measureText(xss);
				canvas.drawText(xss,kx(x)-voi/2,ky(0)+20,paint);
				if(x!=0) canvas.drawText(x2s,kx(-x)-(int)paint.measureText(x2s)/2,ky(0)+20,paint);
				voi+=kx(x)-voi/2;
			}
			paint.setAntiAlias(false);
		}
		for(y=0;y<ys;y+=ystep) {
			paint.setColor(Color.GRAY);	
			if(y>ymin && y<ymax) canvas.drawLine(kx(xmin),ky(y),kx(xmax),ky(y), paint);
			if(-y>ymin && -y<ymax) canvas.drawLine(kx(xmin),ky(-y),kx(xmax),ky(-y), paint);
			paint.setColor(Color.WHITE);
			paint.setAntiAlias(true);
			if(y>ymin && y<ymax) canvas.drawLine(kx(0)+2,ky(y),kx(0)-2,ky(y), paint);
			if(-y>ymin && -y<ymax) canvas.drawLine(kx(0)+2,ky(-y),kx(0)-2,ky(-y),paint);
			xss=""+y;
			x2s=""+(-y);
			voi=(int)paint.measureText(xss);
			canvas.drawText(xss,kx(xmin)-voi,ky(y)+8,paint);
			if(y!=0) canvas.drawText(x2s,kx(xmin)-(int)paint.measureText(x2s),ky(-y)+8,paint);
			paint.setAntiAlias(false);
		}
		// Koordinatenachsen
		paint.setColor(Color.WHITE);
		if(Math.signum(xmin)!=Math.signum(xmax)) 
			canvas.drawLine(kx(0),ky(ymin),kx(0),ky(ymax),paint);

		if(Math.signum(ymin)!=Math.signum(ymax))  
			canvas.drawLine(kx(xmin),ky(0),kx(xmax),ky(0),paint);

		for(i=-(int)textsize/2;i<=textsize/2;i++) {
			// Pfeile
			if(Math.signum(xmin)!=Math.signum(xmax)) 
				canvas.drawLine(kx(0)+i,ky(ymax)+textsize,kx(0),ky(ymax),paint);
			if(Math.signum(ymin)!=Math.signum(ymax))
				canvas.drawLine(kx(xmax)-textsize,ky(0)+i,kx(xmax),ky(0),paint);

		}
		// Beschriftung
		paint.setAntiAlias(true);
		canvas.drawText(ex,bx+bw-(int)paint.measureText(ex),ky(0)-8,paint);
		canvas.drawText(ey,Math.max(0,kx(0)-(int)paint.measureText(ex)-8),by+15,paint);
		// Daten
		paint.setStrokeWidth(2);
		paint.setColor(Color.RED);	
		double data,odata=0;
		for(i=0;i<1500000;i+=1000) {
			data=LaufzeitActivity.data[(i+1000)/1000];
			//    	if(data<0) {LaufzeitActivity.maxkap=i-1000; break;}
			canvas.drawLine(kx(i),ky(odata),kx(i+1000),ky(data),paint);
			odata=data;
		}    
		paint.setAntiAlias(false);
		paint.setStrokeWidth(0);
	}
}
